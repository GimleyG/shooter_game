#include "stdafx.h"
#include "controllers\player_controller.h"

using namespace shooter;

void PlayerController::setTankToControl(Tank* tank)
{
    tank_ = tank;
}

Tank::TankStateChange shooter::PlayerController::updateState(float elapsedTime)
{
    Tank::TankStateChange result{ sf::Vector2f(), tank_->getAngle() };

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
    {
        tank_->moveForward(elapsedTime, result.posDelta);
    }
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
    {
        tank_->moveBackward(elapsedTime, result.posDelta);
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
    {
        tank_->turnLeft(elapsedTime, result.newAngle);
    }
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
    {
        tank_->turnRight(elapsedTime, result.newAngle);
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
    {
        tank_->shoot();
    }

    return result;
}
