#include "stdafx.h"
#include "controllers\ai_map.h"

namespace shooter
{

AIMap::AIMap() 
{
    auto p1 = std::make_shared<PathPoint>(0);
    auto p2 = std::make_shared<PathPoint>(1);
    auto p3 = std::make_shared<PathPoint>(2);
    auto p4 = std::make_shared<PathPoint>(3);
    auto p5 = std::make_shared<PathPoint>(4);
    auto p6 = std::make_shared<PathPoint>(5);
    auto p7 = std::make_shared<PathPoint>(6);
    auto p8 = std::make_shared<PathPoint>(7);

    p1->position = { 40, 80 };
    p2->position = { 284, 80 };
    p3->position = { 370, 236 };
    p4->position = { 476, 368 };
    p5->position = { 476, 494 };
    p6->position = { 209, 494 };
    p7->position = { 133, 368 };
    p8->position = { 40, 236 };

    p1->neighbors = { p2, p8 };
    p2->neighbors = { p3, p1 };
    p3->neighbors = { p8, p2, p4 };
    p4->neighbors = { p5, p3, p7 };
    p5->neighbors = { p6, p4 };
    p6->neighbors = { p7, p5 };
    p7->neighbors = { p4, p8, p6 };
    p8->neighbors = { p7, p3, p1 };
    points = { p1, p2, p3, p4, p5, p6, p7, p8 };
    lastPoint_ = p1;
}

PathPointPtr AIMap::nearestPathPoint(sf::Vector2f pos)
{
    float dist = 1000;
    PathPointPtr res;
    for (auto p : points)
    {
        auto distance = std::sqrtf(std::pow(p->position.x - pos.x, 2) + std::pow(p->position.y - pos.y, 2));
        if (distance < dist)
        {
            dist = distance;
            res = p;
        }
    }

    return res;
}
PathPointPtr AIMap::nextPointOnPathTo(sf::Vector2f self_pos, PathPointPtr destination)
{
    static auto distance = [](sf::Vector2f p1, sf::Vector2f p2) 
        {
            return std::sqrtf(std::pow(p1.x - p2.x, 2) + std::pow(p1.y - p2.y, 2));;
        };

    if (lastPoint_->id == destination->id)
    {
        return lastPoint_;
    }

    if (distance(self_pos, lastPoint_->position) < 10) 
    {
        for (auto p : lastPoint_->neighbors) 
        {
            if (p->id == destination->id)
            {
                return p;
            }
        }

        // simply choose one of the neighbors
        std::random_device rd;
        std::mt19937 eng(rd());
        std::uniform_int_distribution<> distr(0, lastPoint_->neighbors.size() - 1);
        lastPoint_ = lastPoint_->neighbors[distr(eng)];

        return lastPoint_;
    }
    else 
    {
        return lastPoint_;
    }
}

} // namespace shooter