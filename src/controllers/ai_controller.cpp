#include "stdafx.h"

#include "controllers/ai_controller.h"
#include "game/game.h"
#include "battlefield.h"

namespace shooter 
{

struct Line 
{
    sf::Vector2f position;
    float        angle;

    float fx(float x) 
    {
        float k = std::tan((angle - 90) * gradToRadCoef);
        return k*(x - position.x) + position.y;
    }

    float xf(float y) 
    {
        float k = std::tan((angle - 90) * gradToRadCoef);
        return (y - position.y) / k + position.x;
    }
};

bool hitPoint(sf::Rect<float> rect, Line line, sf::Vector2f &point) 
{
    std::vector<sf::Vector2f> sides(4, { 1000,1000 });

    if (rect.contains({ rect.left + 1, line.fx(rect.left) })) 
    {
        sides[0] = { rect.left, line.fx(rect.left) };
    }
    else if (rect.contains({ rect.left + rect.width - 1, line.fx(rect.left + rect.width) })) 
    {
        sides[1] = { rect.left + rect.width, line.fx(rect.left + rect.width) };
    }
    else if (rect.contains({ line.xf(rect.top), rect.top + 1 })) 
    {
       sides[2] = { line.xf(rect.top), rect.top };
    }
    else if (rect.contains({ line.xf(rect.top + rect.height), rect.top + rect.height - 1 })) 
    {
        sides[3] = { line.xf(rect.top + rect.height), rect.top + rect.height };
    }
    else
    {
        return false;
    }

    float dist = 1000;
    
    for (auto side : sides) 
    {
        if (dist > std::sqrtf(std::pow(side.x - line.position.x, 2) + std::pow(side.y - line.position.y, 2))) 
        {
            dist = std::sqrtf(std::pow(side.x - line.position.x, 2) + std::pow(side.y - line.position.y, 2));
            point.x = side.x;
            point.y = side.y;
        }
    }

    return true;
}


float AIController::angleTo(sf::Vector2f point) 
{
    auto pos   = tank_->getPosition();
    auto angle = 90 + (1 / gradToRadCoef) * std::atan2((point.y - pos.y), (point.x - pos.x));
    
    if (angle > 180)
    {
        angle -= 360;
    }

    return angle;
}

Tank::TankStateChange AIController::turnTo(sf::Vector2f point, float elapsedTime) 
{
    auto tank_angle = tank_->getAngle();

    Tank::TankStateChange result{ sf::Vector2f(), tank_angle};

    auto angle = angleTo(point);

    float delta       = tank_angle - angle;
    bool deltaTooHigh = abs(delta) > 180;

    if ((tank_angle > angle) && (!deltaTooHigh) || (tank_angle < 0) && deltaTooHigh) 
    {
        tank_->turnLeft(elapsedTime, result.newAngle);
    }
    else if (((tank_angle < angle) && (!deltaTooHigh)) || ((tank_angle > 0) && deltaTooHigh)) 
    {
        tank_->turnRight(elapsedTime, result.newAngle);
    }

    return result;
}

Tank::TankStateChange AIController::moveTo(sf::Vector2f point, float elapsedTime) 
{
    auto result = turnTo(point, elapsedTime);

    auto tank_angle = tank_->getAngle();
    auto angle      = angleTo(point);

    if (abs(tank_angle) > (abs(angle) - 5) && abs(tank_angle) < (abs(angle) + 5))
    {
        tank_->moveForward(elapsedTime, result.posDelta);
    }

    return result;
}

Tank::TankStateChange AIController::updateState(float elapsedTime) 
{
    Tank::TankStateChange result{ sf::Vector2f(), tank_->getAngle() };

    auto player     = Game::getGameObjectManager().get("Player");
    auto player_pos = player->getPosition();
    auto pos        = tank_->getPosition();
    auto angle      = angleTo(player_pos);

    bool can_shoot         = true;
    bool tooCloseToBarrier = false;

    // check if there is no barricade to block a bullet
    auto battlefield = static_cast<Battlefield*>(Game::getGameObjectManager().get("Battlefield"));
    
    for (auto barrier : battlefield->getBarricades()) 
    {
        auto rect = barrier.getGlobalBounds();
        auto dist_to_player = std::sqrtf(std::pow(player_pos.x - pos.x, 2) + std::pow(player_pos.y - pos.y, 2));

        if (rect.intersects(tank_->getBoundingRect()))
        {
            tooCloseToBarrier = true;
        }

        sf::Vector2f point;
        if (!hitPoint(rect, { pos, angle }, point))
        {
            continue;
        }

        auto dist_to_hitpoint = std::sqrtf(std::pow(point.x - pos.x, 2) + std::pow(point.y - pos.y, 2));
        if (dist_to_player > dist_to_hitpoint) 
        {
            can_shoot = false;
            break;
        }
    }

    if (tooCloseToBarrier) 
    {
        tank_->moveBackward(elapsedTime, result.posDelta);
        return result;
    }

    sf::Vector2f point;
    if (can_shoot) 
    {
        result = turnTo(player_pos, elapsedTime);
        auto tank_angle = tank_->getAngle();
        if (abs(tank_angle) > (abs(angle) - 5) && abs(tank_angle) < (abs(angle) + 5))
        {
            tank_->shoot();
        }
    }
    else 
    { //seek for player		
        auto player_pp = map_.nearestPathPoint(player_pos);
        auto next_pp = map_.nextPointOnPathTo(pos, player_pp);
        result = moveTo(next_pp->position, elapsedTime);
    }

    return result;
}

void AIController::setTankToControl(Tank* tank) 
{
    tank_ = tank;
}

} // namespace shooter 
