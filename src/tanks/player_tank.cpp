#include "stdafx.h"

#include "battlefield.h"
#include "game/game.h"
#include "tanks/bullet.h"
#include "tanks/ai_tank.h"
#include "tanks/player_tank.h"

namespace shooter {

PlayerTank::PlayerTank(Controller *ctrl):
    controller_(ctrl)
{
    load("images/player_tank.png");
    assert(isLoaded());

    angle_ = 0;

    controller_->setTankToControl(this);

    getSprite().setPosition(Game::ScreenWidth / 2, Game::ScreenHeight - 50);
    getSprite().setOrigin(getSprite().getLocalBounds().width / 2.f, 
                         (getSprite().getLocalBounds().height + 12.f)/ 2.f); // 12 here is a length of a tanks's barrel
}

PlayerTank::~PlayerTank()
{
    if (controller_)
    {
        delete controller_;
    }
}


PlayerTank::TankStateChange PlayerTank::updateState(float elapsedTime)
{
    return controller_->updateState(elapsedTime);
}

bool PlayerTank::isStateChangeValid(TankStateChange state_change)
{
    auto pos = getPosition();
    pos.x += state_change.posDelta.x;
    pos.y += state_change.posDelta.y;

    if ((pos.x - radius_ < 0) || (pos.x > (Game::ScreenWidth - radius_)) ||
        (pos.y - radius_ < 0) || (pos.y > (Game::ScreenHeight - radius_)))
    {
        return false;
    }

    auto battlefield = static_cast<Battlefield*>(Game::getGameObjectManager().get("Battlefield"));
    for (auto barrier : battlefield->getBarricades()) 
    {
        auto rect = barrier.getGlobalBounds();
        if ((pos.x + radius_> rect.left) && (pos.x - radius_ < rect.left + rect.width) &&
            (pos.y + radius_ > rect.top) && (pos.y - radius_ < rect.top + rect.height)) 
        {
            return false;
        }
    }

    AITank* bot = dynamic_cast<AITank*>(Game::getGameObjectManager().get("AI"));
    if (bot != nullptr)
    {
        auto bot_pos    = bot->getPosition();
        auto bot_radius = bot->getRadius();

        auto dist = std::sqrtf(std::pow(bot_pos.x - pos.x, 2) + std::pow(bot_pos.y - pos.y, 2));
        if (dist < bot_radius + radius_)
        {
            return false;
        }
    }

    return true;
}

} // namespace shooter
