#include "stdafx.h"

#include "battlefield.h"
#include "game/game.h"
#include "tanks/ai_tank.h"
#include "tanks/bullet.h"
#include "tanks/player_tank.h"

namespace shooter
{

int Bullet::Id = 0;

Bullet::Bullet() :
    Bullet(0) 
{
}

Bullet::Bullet(float angle, const sf::Vector2f &initPos) :
    radius_(3), 
    velocity_(400.0f), 
    angle_(angle), 
    name_{ "blt" + std::to_string(Id) }, 
    previouslyBouncedId_(-1), 
    initPos_(initPos)
{
    Id = (Id + 1) % 100;
    load("images/bullet.png");
    assert(isLoaded());

    getSprite().setOrigin(3, 3);
    setPosition(initPos_);
}

void Bullet::update(float elapsedTime)
{
    auto state = updateState(elapsedTime);

    if (isHitAI(state))
    {
        Game::scoreTo(GameSide::Player);
        return;
    }
    else if (isHitPlayer(state))
    {
        Game::scoreTo(GameSide::Computer);
        return;
    }

    if (!isStateChangeValid(state))
    {
        Game::getGameObjectManager().queueForDeletion(name_);
        return;
    }

    angle_ = state.newAngle;
    getSprite().move(state.posDelta);
}

Bullet::BulletStateChange Bullet::updateState(float elapsedTime)
{
    BulletStateChange result{ sf::Vector2f(), angle_ };
    float moveAmount = velocity_  * elapsedTime;

    auto sin = std::sin(result.newAngle * gradToRadCoef);
    auto cos = std::cos(result.newAngle * gradToRadCoef);

    result.posDelta.x = sin * moveAmount;
    result.posDelta.y = -cos * moveAmount;

    correctStateIfBounced(result);

    return result;
}

void Bullet::correctStateIfBounced(BulletStateChange& state)
{
    auto pos = getPosition();
    pos.x += state.posDelta.x;
    pos.y += state.posDelta.y;

    auto battlefield = static_cast<Battlefield*>(Game::getGameObjectManager().get("Battlefield"));

    for (auto barrier : battlefield->getBarricades())
    {
        auto bar_bound = barrier.getGlobalBounds();

        bool collide = (pos.x + radius_) > bar_bound.left &&
                       (pos.x - radius_) < (bar_bound.left + bar_bound.width) &&
                       (pos.y + radius_) > bar_bound.top &&
                       (pos.y - radius_) < (bar_bound.top + bar_bound.height);

        if (collide && previouslyBouncedId_ != barrier.getId()) 
        {
            CollideSide sideLR{ CollideSide::None }, sideTB{ CollideSide::None };

            if (initPos_.x < bar_bound.left) sideLR = CollideSide::Left;
            if (initPos_.x > bar_bound.left + bar_bound.width) sideLR = CollideSide::Right;
            if (initPos_.y < bar_bound.top) sideTB = CollideSide::Top;
            if (initPos_.y > bar_bound.top + bar_bound.height) sideTB = CollideSide::Bottom;

            bool leftOrRight = (sideLR == CollideSide::None) ? false :
                pos.x < bar_bound.left || pos.x >(bar_bound.left + bar_bound.width);
            bool topOrBottom = (sideTB == CollideSide::None) ? false :
                pos.y <= bar_bound.top || pos.y >= (bar_bound.top + bar_bound.height);

            if (leftOrRight)
            {
                state.newAngle *= -1;
                state.posDelta.x *= -1;
            }
            else if (topOrBottom)
            {
                state.newAngle = 180 - state.newAngle;
                if (state.newAngle < -180)
                {
                    state.newAngle += 360;
                }
                state.posDelta.y *= -1;
            }

            previouslyBouncedId_ = barrier.getId();
            break;
        }
    }
}

bool Bullet::isHitPlayer(BulletStateChange state) {
	auto pos = getPosition();

	PlayerTank* player = dynamic_cast<PlayerTank*>(Game::getGameObjectManager().get("Player"));
	if (player != nullptr) {
		auto player_pos = player->getPosition();
		auto player_radius = player->getRadius();

		auto dist = std::sqrtf(std::pow(player_pos.x - pos.x, 2) + std::pow(player_pos.y - pos.y, 2));
		if (dist < player_radius + radius_) {
			return true;
		}
	}
	return false;
}

bool Bullet::isHitAI(BulletStateChange state) {
	auto pos = getPosition();
	AITank* bot = dynamic_cast<AITank*>(Game::getGameObjectManager().get("AI"));
	if (bot != nullptr) {
		auto bot_pos = bot->getPosition();
		auto bot_radius = bot->getRadius();

		auto dist = std::sqrtf(std::pow(bot_pos.x - pos.x, 2) + std::pow(bot_pos.y - pos.y, 2));
		if (dist < bot_radius + radius_) {
			return true;
		}
	}
	return false;
}

bool Bullet::isStateChangeValid(BulletStateChange state)
{
    auto pos = getPosition();
    pos.x += state.posDelta.x;
    pos.y += state.posDelta.y;

    //collide with the left and right side of the screen
    if ((pos.x <= radius_) || (pos.x + radius_ >= Game::ScreenWidth) ||
        (pos.y - radius_ <= 0) || (pos.y + radius_ >= Game::ScreenHeight))
    {
        return false;
    }

    auto battlefield = static_cast<Battlefield*>(Game::getGameObjectManager().get("Battlefield"));
    auto bound = getBoundingRect();

    for (auto barrier : battlefield->getBarricades())
    {
        auto bar_bound = barrier.getGlobalBounds();
        // if inside barricade
        if (pos.x > bar_bound.left &&
            pos.x < (bar_bound.left + bar_bound.width) &&
            pos.y > bar_bound.top &&
            pos.y < (bar_bound.top + bar_bound.height))
        {
            return false;
        }
    }

    return true;
}

} // namespace shooter
