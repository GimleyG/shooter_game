#include "stdafx.h"

#include "game/game.h"
#include "tanks/bullet.h"
#include "tanks/tank.h"

namespace shooter
{

Tank::Tank() :
    radius_(21),
    timePassedSinceLastShot_(0) 
{}

void Tank::update(float elapsedTime)
{
    timePassedSinceLastShot_ += elapsedTime;
    auto state_change = updateState(elapsedTime);

    if (!isStateChangeValid(state_change))
    {
        return;
    }

    getSprite().move(state_change.posDelta.x, state_change.posDelta.y);
    angle_ = state_change.newAngle;
    getSprite().setRotation(angle_);
}

void Tank::turnLeft(float elapsedTime, float& angle)
{
    angle -= 40.f * elapsedTime;
    if (angle < -180)
    {
        angle += 360;
    }
}

void Tank::turnRight(float elapsedTime, float& angle)
{
    angle += 40.0f * elapsedTime;
    if (angle > 180)
    {
        angle -= 360;
    }
}

void Tank::moveForward(float elapsedTime, sf::Vector2f& posDelta)
{
    auto moveAmount = 100.f * elapsedTime;
    auto sin = std::sin(angle_ * gradToRadCoef);
    auto cos = std::cos(angle_ * gradToRadCoef);

    posDelta.x = sin*moveAmount;
    posDelta.y = -cos*moveAmount;
}

void Tank::moveBackward(float elapsedTime, sf::Vector2f& posDelta)
{
    auto moveAmount = -100.f * elapsedTime;
    auto sin = std::sin(angle_ * gradToRadCoef);
    auto cos = std::cos(angle_ * gradToRadCoef);
    posDelta.x = sin*moveAmount;
    posDelta.y = -cos*moveAmount;
}

void Tank::shoot()
{
    if (timePassedSinceLastShot_ < shootDelay_)
    {
        return;
    }

    timePassedSinceLastShot_ = 0;

    float sin = std::sin(angle_ * gradToRadCoef);
    float cos = std::cos(angle_ * gradToRadCoef);
    
    auto pos   = this->getPosition();
    auto bound = this->getBoundingRect();

    Bullet* blt = new Bullet(angle_, { pos.x + sin*(bound.width / 2.f), pos.y - cos*(bound.height / 2.f) });
    Game::getGameObjectManager().add(blt->getName(), blt);
}

} // namespace shooter 
