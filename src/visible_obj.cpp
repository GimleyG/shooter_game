#include "stdafx.h"
#include "visible_obj.h"

namespace shooter
{

VisibleObject::VisibleObject()
	: isLoaded_(false)
{}

void VisibleObject::load(const std::string& filename)
{
    isLoaded_ = image_.loadFromFile(filename);
    if (isLoaded_)
    {
        filename_ = filename;
        sprite_.setTexture(image_);
    }
    else
    {
        filename_ = "";
    }
}

void VisibleObject::draw(sf::RenderWindow& renderWindow)
{
    if (isLoaded_)
    {
        renderWindow.draw(sprite_);
    }
}

void VisibleObject::update(float elapsedTime)
{
    // do nothing
}

sf::Vector2f VisibleObject::getPosition() const
{
    if (isLoaded_)
    {
        return sprite_.getPosition();
    }

    return sf::Vector2f();
}

void VisibleObject::setPosition(float x, float y)
{
    if (isLoaded_)
    {
        sprite_.setPosition(x, y);
    }
}

void VisibleObject::setPosition(const sf::Vector2f &pos)
{
    if (isLoaded_)
    {
        sprite_.setPosition(pos);
    }
}

sf::Rect<float> VisibleObject::getBoundingRect() const
{
    return sprite_.getGlobalBounds();
}

} // namespace shooter 
