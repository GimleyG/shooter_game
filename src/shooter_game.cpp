// shooter_game.cpp : Defines the entry point for the console application.

#include "stdafx.h"
#include "game/game.h"

int main()
{
    shooter::Game::start();

    return 0;
}
