#include "stdafx.h"
#include "menu/splash_screen.h"

namespace shooter 
{

void SplashScreen::show(sf::RenderWindow& renderWindow)
{
    sf::Texture image;
    if (image.loadFromFile("images/SplashScreen.png") != true)
    {
        return;
    }

    sf::Sprite sprite(image);
    renderWindow.draw(sprite);
    renderWindow.display();

    sf::Event event;
    while (true)
    {
        while (renderWindow.pollEvent(event))
        {
            if (event.type == sf::Event::EventType::KeyPressed
                || event.type == sf::Event::EventType::MouseButtonPressed
                || event.type == sf::Event::EventType::Closed)
            {
                return;
            }
        }
    }
}

} // namespace shooter 
