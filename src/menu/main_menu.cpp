#include "stdafx.h"
#include "menu/main_menu.h"

namespace shooter 
{

MenuResult MainMenu::show(sf::RenderWindow& window) 
{
	//Load menu image from file
	sf::Texture image;
	image.loadFromFile("images/mainmenu.png");
	menuSprite_.setTexture(image);
	
	//Setup clickable regions
	//Play menu item coordinates
	MenuItem playButton;	
	playButton.rect.top = 243;
	playButton.rect.width = 131;
	playButton.rect.left = 183;
	playButton.rect.height = 56;
	playButton.action = MenuResult::Play;
	
	//Exit menu item coordinates
	MenuItem exitButton;
	exitButton.rect.top = 331;
	exitButton.rect.width = 109;
	exitButton.rect.left = 189;
	exitButton.rect.height = 57;
	exitButton.action = MenuResult::Exit;

	menuItems_.push_back(playButton);
	menuItems_.push_back(exitButton);

	miPtr_.load("images/pointer.png");
	miPtr_.setUp(menuItems_.begin(), std::prev(menuItems_.end()));
	
	update(window);
	
	return getMenuResponse(window);
}

MenuResult  MainMenu::getMenuResponse(sf::RenderWindow& window) 
{
    sf::Event menuEvent;
    while (true) 
    {
        while (window.pollEvent(menuEvent)) 
        {
            if (menuEvent.type == sf::Event::KeyPressed) 
            {
                if (menuEvent.key.code == sf::Keyboard::Return)
                {
                    return miPtr_.getCurrent().action;
                }
                else if (menuEvent.key.code == sf::Keyboard::Up) 
                {
                    miPtr_.prev();
                    update(window);
                }
                else if (menuEvent.key.code == sf::Keyboard::Down) 
                {
                    miPtr_.next();
                    update(window);
                }
            }

            if (menuEvent.type == sf::Event::Closed)
            {
                return MenuResult::Exit;
            }
        }
    }
}

void MainMenu::update(sf::RenderWindow& window)
{
    window.clear(sf::Color(0, 0, 0));
    window.draw(menuSprite_);
    miPtr_.draw(window);
    window.display();
}


void MainMenu::MenuItemPointer::next()
{
    if (current_ == end_)
    {
        current_ = begin_;
    }
    else
    {
        current_++;
    }

    updatePosition();
}

void MainMenu::MenuItemPointer::prev()
{
    if (current_ == begin_)
    {
        current_ = end_;
    }
    else
    {
        current_--;
    }

    updatePosition();
}

void MainMenu::MenuItemPointer::setUp(MenuItmIter beg, MenuItmIter end)
{
    begin_   = beg; 
    end_     = end; 
    current_ = beg; 

    updatePosition();
}

void MainMenu::MenuItemPointer::updatePosition()
{
    auto rect = current_->rect;
    setPosition(rect.left - 30, rect.top + rect.height / 2 - 10);
}

} // namespace shooter
