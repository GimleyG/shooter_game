#include "stdafx.h"

#include "battlefield.h"

namespace shooter 
{

Battlefield::Battlefield() 
{
    Barricade bar({ 177.f, 39.f });
    bar.setFillColor(sf::Color(250, 150, 0));

    bar.setPosition(77.f, 147.f);
    barricades_.push_back(bar);

    bar.setPosition(163.f, 279.f);
    bar.setId(1);
    barricades_.push_back(bar);

    bar.setPosition(239.f, 405.f);
    bar.setId(2);
    barricades_.push_back(bar);	
}

void Battlefield::draw(sf::RenderWindow & window) 
{
    for (auto b : barricades_)
    {
        window.draw(b);
    }
}

} // namespace shooter 
