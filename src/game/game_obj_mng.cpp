#include "stdafx.h"

#include "battlefield.h"
#include "controllers/ai_controller.h"
#include "controllers/player_controller.h"
#include "tanks/ai_tank.h"
#include "game/game.h"
#include "game/game_obj_mng.h"
#include "tanks/player_tank.h"

namespace shooter
{

GameObjectManager::~GameObjectManager()
{
    std::for_each(gameObjects_.begin(), gameObjects_.end(), [](const auto& p)
        {
            delete p.second;
        });
}

void GameObjectManager::add(const std::string& name, VisibleObject* gameObject)
{
    gameObjects_.insert({ name, gameObject });
}

void GameObjectManager::queueForDeletion(const std::string& name)
{
    deletionQueue_.push_back(name);
}

void GameObjectManager::remove(const std::string& name)
{
    auto results = gameObjects_.find(name);
    if (results != gameObjects_.end()) 
    {
        delete results->second;
        gameObjects_.erase(results);
    }
}

VisibleObject* GameObjectManager::get(const std::string& name) const 
{
    auto results = gameObjects_.find(name);
    if (results == gameObjects_.end())
    {
        return nullptr;
    }

    return results->second;
}

void GameObjectManager::init() 
{
    Battlefield* btl = new Battlefield;

    Controller*  pctrl = new PlayerController();
    PlayerTank* player = new PlayerTank(pctrl);

    Controller* ctrl  = new AIController();
    AITank* ai_enemy  = new AITank(ctrl);

    this->add("Battlefield", btl);
    this->add("Player",      player);
    this->add("AI",          ai_enemy);
}

void GameObjectManager::reset()
{
    remove("Player");
    remove("AI");

    for (auto it = gameObjects_.begin(); it != gameObjects_.end();)
    {
        if (it->first == "Battlefield")
        {
            ++it;
        }
        else
        {
            it = gameObjects_.erase(it);
        }
    }

    Controller* pctrl  = new PlayerController();
    PlayerTank* player = new PlayerTank(pctrl);

    Controller* ctrl  = new AIController();
    AITank*  ai_enemy = new AITank(ctrl);

    add("Player", player);
    add("AI",     ai_enemy);
}

int GameObjectManager::getObjectCount() const 
{
    return gameObjects_.size();
}

void GameObjectManager::drawAll(sf::RenderWindow& renderWindow) 
{
    if (!deletionQueue_.empty()) {
        for (auto name : deletionQueue_)
        {
            remove(name);
        }

        deletionQueue_.clear();
    }

    for (auto itm : gameObjects_)
    {
        itm.second->draw(renderWindow);
    }
}

void GameObjectManager::updateAll()
{
    float timeDelta = clock_.restart().asSeconds();

    for (auto itm : gameObjects_)
    {
        itm.second->update(timeDelta);
    }
}

} // namespace shooter 
