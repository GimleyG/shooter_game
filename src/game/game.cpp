#include "stdafx.h"

#include "game/game.h"
#include "menu/main_menu.h"
#include "menu/splash_screen.h"

namespace shooter 
{

GameState Game::gameState_ = GameState::Uninitialized;
sf::RenderWindow Game::mainWindow_;
GameObjectManager Game::gameObjectManager_;
Game::Score Game::score_ = { 0, 0 };

void Game::start() 
{
    if (gameState_ != GameState::Uninitialized)
    {
        return;
    }

    mainWindow_.create(sf::VideoMode(ScreenWidth, ScreenHeight, 32), "The Shooter");
    gameObjectManager_.init();

    gameState_ = GameState::ShowingSplash;

    while (!isExiting()) 
    {
        gameLoop();
    }

    mainWindow_.close();
}

bool Game::isExiting()
{
    return gameState_ == GameState::Exiting;
}

void Game::gameLoop() 
{
    sf::Event currentEvent;
    mainWindow_.pollEvent(currentEvent);

    switch (gameState_) 
    {
        case GameState::ShowingMenu:
            showMenu();
            break;
        case GameState::ShowingSplash:
            showSplashScreen();
            break;
        case GameState::Reset:
            gameObjectManager_.reset();
            gameState_ = GameState::Playing;
            break;
        case GameState::Playing:
            mainWindow_.clear(sf::Color(0, 0, 0));

            gameObjectManager_.updateAll();
            gameObjectManager_.drawAll(mainWindow_);
            showCount();

            mainWindow_.display();

            if (currentEvent.type == sf::Event::Closed) 
            {
                gameState_ = GameState::Exiting;
            }

            if (currentEvent.type == sf::Event::KeyPressed) 
            {
                if (currentEvent.key.code == sf::Keyboard::Escape) 
                    showMenu();
            }
            break;
    }
}

void Game::scoreTo(GameSide side) 
{
    if (side == GameSide::Player)
    {
        score_.first += 1;
    }
    else
    {
        score_.second += 1;
    }

    gameState_ = GameState::Reset;
}

void Game::showSplashScreen() 
{
    SplashScreen splashScreen;
    splashScreen.show(mainWindow_);
    gameState_ = GameState::ShowingMenu;
}

void Game::showMenu() 
{
    MainMenu mainMenu;
    auto result = mainMenu.show(mainWindow_);
    switch (result) 
    {
        case MenuResult::Exit:
            gameState_ = GameState::Exiting;
            break;
        case MenuResult::Play:
            gameState_ = GameState::Playing;
            break;
    }
}

void Game::showCount() 
{
    sf::Font font;
    font.loadFromFile("BAUHS93.TTF");
    
    sf::Text text;
    text.setFont(font);
    text.setString(std::to_string(score_.first) + ":" + std::to_string(score_.second));
    text.setPosition(0, 0);
    text.setColor(sf::Color::Red); 
    text.setCharacterSize(24);

    mainWindow_.draw(text);
}

} // namespace shooter 
