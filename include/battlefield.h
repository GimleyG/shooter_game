#pragma once

#include "visible_obj.h"

namespace shooter 
{

/**
*	@brief Barricade is just RectangleShape with id. Id uses in bullet's bounce logic
*/
class Barricade final : public sf::RectangleShape 
{
public:
    
    Barricade(int id = 0) : RectangleShape(), id_(id) { }

    Barricade(const sf::Vector2f& size, int id = 0) : RectangleShape(size), id_(id) { }
    
    Barricade(const Barricade& b) : RectangleShape(b) { id_ = b.id_; }
    
    ~Barricade() override = default;

    int getId() { return id_; }

    void setId(int id) { id_ = id; }

private:

    int id_;

};

/**
*	@brief Represents map of barricades
*/
class Battlefield final : public VisibleObject 
{
public:

    Battlefield();

    ~Battlefield() = default;

public: // VisibleObject

    void draw(sf::RenderWindow &window) override;

public:

    const std::vector<Barricade>& getBarricades() const { return barricades_; }

private:

    std::vector<Barricade> barricades_;

};

} // namespace shooter 