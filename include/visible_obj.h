#pragma once

namespace shooter 
{

/**
*   @brief Base class for every visible object in the game
*/
class VisibleObject 
{
public:

    VisibleObject();

    virtual ~VisibleObject() = default;

    virtual void load(const std::string &filename);
    virtual void draw(sf::RenderWindow &window);
    virtual void update(float elapsedTime);

    virtual void setPosition(float x, float y);
    virtual void setPosition(const sf::Vector2f&);
    virtual sf::Vector2f getPosition() const;

    virtual bool isLoaded() const { return isLoaded_; }

    virtual float getWidth() const  { return sprite_.getGlobalBounds().width;  }
    virtual float getHeight() const { return sprite_.getGlobalBounds().height; }

    virtual sf::Rect<float> getBoundingRect() const;

protected:

    sf::Sprite& getSprite() { return sprite_; }

private:

    sf::Sprite  sprite_;
    sf::Texture image_;
    std::string filename_;

    bool isLoaded_;

};

} // namespace shooter