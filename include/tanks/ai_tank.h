#pragma once

#include "controllers/controller.h"
#include "tanks/tank.h"
#include "visible_obj.h"

namespace shooter 
{
/**
*   @brief Ai controlled tank
*/
class AITank final : public Tank 
{
public:
    
    AITank(Controller *);
    
    ~AITank() override;

private: // Tank

    TankStateChange updateState(float elapsedTime) override;

    bool isStateChangeValid(TankStateChange) override;

private:

    Controller* controller_;

};

} // namespace shooter 
