#pragma once

#include "visible_obj.h"

namespace shooter 
{

/**
*   @brief Base class for every tank
*/
class Tank : public VisibleObject 
{
public:

    Tank();

    ~Tank() override = default;

    float getRadius() { return radius_; }
    float getAngle()  { return angle_; }

    virtual void update(float elapsedTime) override final;

    struct TankStateChange 
    {
        sf::Vector2f posDelta;
        float        newAngle;
    };

    void turnLeft(float elapsedTime, float& angle);

    void turnRight(float elapsedTime, float& angle);

    void moveForward(float elapsedTime, sf::Vector2f &posDelta);

    void moveBackward(float elapsedTime, sf::Vector2f &posDelta);

    void shoot();

protected:

    virtual TankStateChange updateState(float elapsedTime) = 0;

    virtual bool isStateChangeValid(TankStateChange) = 0;

protected:
    
    float radius_;
    float angle_;
    float timePassedSinceLastShot_;

    const float shootDelay_ = 0.4f;

};

} // namespace shooter 
