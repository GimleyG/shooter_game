#pragma once

#include "visible_obj.h"

namespace shooter 
{

/**
*	@brief The side of barricade that bullet collide with
*/
enum class CollideSide 
{
    None,
    Left,
    Right,
    Top,
    Bottom
};

/**
*	@brief Represents bullet
*/
class Bullet final : public VisibleObject 
{
public:
    
    Bullet();
    
    Bullet(float angle, const sf::Vector2f &initPos = sf::Vector2f(0, 0));

    ~Bullet() override = default;

public: // VisibleObject

    void update(float) override;

public:

    std::string getName() { return name_; }

    static int Id;

    struct BulletStateChange 
    {
        sf::Vector2f posDelta;
        float        newAngle;
    };

private:

    BulletStateChange updateState(float elapsedTime);
    bool isStateChangeValid(BulletStateChange);
    void correctStateIfBounced(BulletStateChange&);

    bool isHitPlayer(BulletStateChange state);
    bool isHitAI(BulletStateChange state);

private:

    float radius_;
    float velocity_;
    float angle_;
    
    std::string  name_;
    sf::Vector2f initPos_;
    int          previouslyBouncedId_;

};

} // namespace shooter 