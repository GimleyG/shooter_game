#pragma once

#include "controllers/controller.h"
#include "tanks/tank.h"
#include "visible_obj.h"

namespace shooter 
{

/**
*   @brief Player controlled tank
*/
class PlayerTank final : public Tank 
{
public:

    explicit PlayerTank(Controller* controller);

    ~PlayerTank() override;

protected: // Tank

    TankStateChange updateState(float elapsedTime) override;

    bool isStateChangeValid(TankStateChange) override;

private:

    Controller* controller_;

};

} // namespace shooter
