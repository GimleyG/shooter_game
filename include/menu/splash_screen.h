#pragma once

namespace shooter 
{

/**
*   @brief Splash screen
*/
class SplashScreen 
{
public:

    void show(sf::RenderWindow& window);

};

} // namespace shooter 
