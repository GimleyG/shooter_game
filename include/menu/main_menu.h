#pragma once

#include <list>

#include "visible_obj.h"

namespace shooter 
{

/**
*   @brief Result of a menu item selection
*/
enum class MenuResult 
{ 
    Nothing, 
    Exit, 
    Play 
};

/**
*   @brief Represents menu of the game
*/
class MainMenu 
{
public:
    /**
    *   @brief Menu item
    */
    struct MenuItem 
    {
        sf::Rect<int> rect;
        MenuResult    action;
    };

    using MenuItmIter = std::list<MenuItem>::iterator;

    /**
    *   @brief Represents pointer that indicates current menu item
    */
    class MenuItemPointer final : public VisibleObject 
    {
    public:
        
        ~MenuItemPointer() override = default;

        void setUp(MenuItmIter beg, MenuItmIter end);

        void prev();
        void next();

        MenuItem getCurrent() { return *current_; }

    private:

        void updatePosition();

    private:

        MenuItmIter begin_;
        MenuItmIter end_;
        MenuItmIter current_;

    };

    MenuResult show(sf::RenderWindow& window);

private:

    MenuResult getMenuResponse(sf::RenderWindow& window);

    void update(sf::RenderWindow& window);

private:

    std::list<MenuItem> menuItems_;
    MenuItemPointer     miPtr_;
    sf::Sprite          menuSprite_;

};

} // namespace shooter