#pragma once

#include "controllers/controller.h"
#include "controllers/ai_map.h"

namespace shooter 
{

/**
*   @brief Ai control logic
*/
class AIController final : public Controller 
{
public:
    
    explicit AIController(Tank* tnk = nullptr) : tank_(tnk) {}
    
    ~AIController() override = default;

public: // Controller

    Tank::TankStateChange updateState(float elapsedTime) override;
    
    void setTankToControl(Tank* ) override;

private:

    Tank::TankStateChange moveTo(sf::Vector2f point, float elapsedTime);

    Tank::TankStateChange turnTo(sf::Vector2f point, float elapsedTime);
    
    float angleTo(sf::Vector2f point);

private:

    Tank* tank_;
    AIMap map_;

};

} // namespace shooter