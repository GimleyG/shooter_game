#pragma once

#include "controllers\controller.h"

namespace shooter 
{

class PlayerController final : public Controller 
{
public:

    PlayerController(Tank* tank = nullptr) : tank_(tank) {}

    ~PlayerController() override = default;

public: // PlayerController

    Tank::TankStateChange updateState(float elapsedTime) override;

    void setTankToControl(Tank *) override;

private:

    Tank *tank_;

};

} // namespace shooter
