#pragma once

#include "tanks/tank.h"

namespace shooter 
{

/**
*	@brief Base class for every tank controller
*/
class Controller 
{
public:

    virtual ~Controller() = default;

    virtual Tank::TankStateChange updateState(float elapsedTime) = 0;

    virtual void setTankToControl(Tank *) = 0;

};

} // namespace shooter