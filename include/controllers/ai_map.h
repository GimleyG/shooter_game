#pragma once

namespace shooter 
{

/**
*	@brief Represents point on the map for ai to move
*/
struct PathPoint 
{
    int id;

    sf::Vector2f position;
    
    std::vector<std::shared_ptr<PathPoint>> neighbors;

    PathPoint() = default;
    PathPoint(int id) { this->id = id; }
};

using PathPointPtr = std::shared_ptr<PathPoint>;


/**
*   @brief Map of points for ai to move
*/
class AIMap 
{
public:

    AIMap();

    /**
    *	@brief Returns nearest point to given position
    *	@param [in] pos current position
    */
    PathPointPtr nearestPathPoint(sf::Vector2f pos);

    /**
    *	@brief Returns next point on the path from current position to destination point
    *	@param [in] self_pos  current position
    *   @param [in] destination   destination point
    */
    PathPointPtr nextPointOnPathTo(sf::Vector2f self_pos, PathPointPtr destination);

public:

    std::vector<PathPointPtr> points;

private:

    PathPointPtr lastPoint_;

};

} // namespace shooter 