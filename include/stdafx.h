#pragma once

#include "targetver.h"

#include <stdio.h>

#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/Audio.hpp>

#include <map>
#include <iostream>
#include <cassert>
#include <memory>
#include <random>

static constexpr float gradToRadCoef = 3.1415926 / 180.0f;
