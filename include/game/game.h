#pragma once

#include "game/game_obj_mng.h"

namespace shooter 
{

/**
*   @brief Possible game states
*/
enum class GameState 
{
    Uninitialized,
    ShowingSplash,
    Reset,
    Paused,
    ShowingMenu,
    Playing,
    Exiting
};

/**
*   @brief Sides in the game to count a score
*/
enum class GameSide 
{
    Player,
    Computer
};

/**
*	@brief Main class of whole game
*/
class Game 
{
public:

    using Score = std::pair<int, int>;

    static constexpr int ScreenWidth  = 512;
    static constexpr int ScreenHeight = 650;

    static void start();
    static void scoreTo(GameSide);
    static GameObjectManager& getGameObjectManager() { return gameObjectManager_; }

private:

    static bool isExiting();
    static void gameLoop();

    static void showSplashScreen();
    static void showMenu();
    static void showCount();

private:

    static Score             score_;
    static GameState         gameState_;
    static sf::RenderWindow  mainWindow_;
    static GameObjectManager gameObjectManager_;

};

} // namespace shooter 