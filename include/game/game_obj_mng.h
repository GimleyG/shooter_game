#pragma once

#include <list>

#include "visible_obj.h"

namespace shooter 
{

/**
*   @brief Represents manager for every visible object in the game
*/
class GameObjectManager 
{
public:

    ~GameObjectManager();

    void add(const std::string& name, VisibleObject* gameObject);
    void queueForDeletion(const std::string& name);
    void remove(const std::string& name);
    int getObjectCount() const;
    VisibleObject* get(const std::string& name) const;

    void init();
    void reset();

    void drawAll(sf::RenderWindow& renderWindow);
    void updateAll();

private:

    std::map<std::string, VisibleObject*> gameObjects_;
    std::list<std::string>                deletionQueue_;

    sf::Clock clock_;

};

} // namespace shooter